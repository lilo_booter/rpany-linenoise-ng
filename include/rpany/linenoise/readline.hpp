#pragma once

extern bool eof;
extern std::weak_ptr< rpany::dictionary::dictionary_type > completer_dictionary;

extern completer_lookup_type completer_lookup;

void completionHook( char const *prefix_ptr, linenoiseCompletions* lc )
{
	// We'll do quite a bit with the prefix as a string here
	const std::string prefix( prefix_ptr );

	// Check provided stack dictionary first for currently known words
	auto dictionary = completer_dictionary.lock( );
	if ( dictionary )
	{
		if ( prefix.find( "::" ) == std::string::npos )
		{
			const auto &lexicon = dictionary->lexicon;
			for ( auto iter = lexicon.lower_bound( prefix ); iter != lexicon.end( ) && iter->first.rfind( prefix, 0 ) == 0; iter ++ )
				linenoiseAddCompletion( lc, ( iter->first + " " ).c_str( ) );
		}
		else if ( prefix.find( "->", 2 ) == std::string::npos )
		{
			const auto &vocab = dictionary->vocab;
			const auto key = prefix.substr( 2 );
			for ( auto iter = vocab.lower_bound( key ); iter != vocab.end( ) && iter->first.find( key ) == 0; iter ++ )
				linenoiseAddCompletion( lc, ( "::" + iter->first + "->" ).c_str( ) );
		}
		else
		{
			const auto &vocab = dictionary->vocab;
			const auto end_of_vocab = prefix.find( "->", 2 );
			const auto key = prefix.substr( 2, end_of_vocab - 2 );
			const auto word = prefix.substr( end_of_vocab + 2 );
			const auto list_iter = vocab.find( key );
			if ( list_iter != vocab.end( ) )
			{
				auto list = list_iter->second;
				for ( auto iter = list.lower_bound( word ); iter != list.end( ) && iter->find( word ) == 0; iter ++ )
					linenoiseAddCompletion( lc, ( "::" + key + "->" + *iter + " " ).c_str( ) );
			}
		}
	}

	// Check additional lookup sets (vml filter:, store: etc)
	for( auto iter = completer_lookup.begin( ); iter != completer_lookup.end( ); iter ++ )
	{
		if ( iter->first == "" || prefix.rfind( iter->first, 0 ) == 0 )
		{
			const auto &dict = iter->second;
			std::string remainder = prefix.substr( iter->first.size( ) );
			auto find = dict.lower_bound( remainder );
			while ( find != dict.end( ) && find->rfind( remainder, 0 ) == 0 )
				linenoiseAddCompletion(lc, ( iter->first + *( find ++ ) ).c_str( ) );
		}
		else if ( iter->first.rfind( prefix, 0 ) == 0 )
		{
			linenoiseAddCompletion( lc, iter->first.c_str( ) );
		}
	}

#if !defined( WIN32 ) && !defined( _WIN32 )
	// Check for file: and treat everything to the right as a directory and file path
	const std::string token = prefix.rfind( "file:" ) == 0 ? "file:" :
							  prefix.rfind( "\"file:" ) != std::string::npos ? "\"file:" :
							  prefix.rfind( ":file:" ) != std::string::npos ? ":file:" :
							  "";
	if ( token != "" )
	{
		const auto pos = prefix.rfind( token ) + token.size( );
		const auto start = prefix.substr( 0, pos );
		const auto file_start = prefix.substr( pos );
		const auto sep_pos = file_start.rfind( '/' );
		const auto dir = file_start.substr( 0, sep_pos != std::string::npos ? sep_pos + 1 : 0 );
		const auto file = file_start.substr( sep_pos != std::string::npos ? sep_pos + 1 : 0 );

		DIR *contents = opendir( dir != "" ? dir.c_str( ) : "." );

		if ( contents )
		{
			struct dirent *entry;
			while( ( entry = readdir( contents ) ) != 0 )
				if ( *( entry->d_name ) != '.' && std::string( entry->d_name ).find( file ) == 0 )
					linenoiseAddCompletion( lc, ( start + dir + std::string( entry->d_name ) + " " ).c_str( ) );
			closedir( contents );
		}
	}
	else if ( std::string( "file:" ).find( prefix ) == 0 )
	{
		linenoiseAddCompletion( lc, "file:" );
	}
#endif
}

void get_dictionary( rpany::stack &s )
{
	lookup_type dict;
	auto count = s.pull< int >( );

	for ( int i = 0; i < count; i ++ )
	{
		std::string token = s.pull< std::string >( );
		auto index = token.find_first_of( ".*" );
		if ( index != std::string::npos )
			token = token.substr( 0, index );
		if ( token != "" )
			dict.insert( token );
	}

	std::string prefix = s.pull< std::string >( );
	completer_lookup[ prefix ] = std::move( dict );
}

inline std::string rl_gets( const std::string &prompt )
{
	if ( eof ) return "";

	// Just to avoid issues where an interrupt was caught in the last command as it terminated
	rpany::reset_interrupt_state( );

	// Get a line from the user
	char *line_read = linenoise( prompt.c_str( ), eof );

	// If the line has any text in it, save it on the history
	if ( !eof && line_read && *line_read && strcmp( line_read, " " ) )
		linenoiseHistoryAdd( line_read );

	std::string result = line_read != 0 ? std::string( line_read ) : "";

	// If the buffer has already been allocated, return the memory to the free pool
	if ( line_read )
	{
		free( line_read );
		line_read = 0;
	}

	return result;
}

const std::string linenoise_history( const std::string &custom = "" )
{
	auto enrolled = rpany::enrolled( );
	if ( custom == "" && enrolled.size( ) == 0 )
		RPANY_THROW( rpany::stack_exception, "No tools enrolled" );
	const char *home = getenv( "HOME" ) != 0 ? getenv( "HOME" ) : getenv( "USERPROFILE" );
	const std::string current = custom == "" ? enrolled.at( enrolled.size( ) - 1 ) : custom;
	if ( !home )
		RPANY_THROW( rpany::stack_exception, "No HOME directory specified" );
	return std::string( home ) + "/." + current + ".rpn";
}

extern std::string history_filename;

void linenoise_history_save( )
{
	if ( history_filename != "" )
	{
		linenoiseHistorySave( history_filename.c_str( ) );
	}
}

void linenoise_history_close( )
{
	linenoise_history_save( );
	linenoiseHistoryFree( );
}

void linenoise_history_load( rpany::stack &s )
{
	if ( history_filename == "" )
	{
		history_filename = linenoise_history( );
		linenoiseHistoryLoad( history_filename.c_str( ) );
		atexit( linenoise_history_close );
	}
}

void linenoise_history_custom_load( rpany::stack &s )
{
	auto custom = s.pull< std::string >( );
	if ( history_filename == "" )
	{
		history_filename = linenoise_history( custom );
		linenoiseHistoryLoad( history_filename.c_str( ) );
		atexit( linenoise_history_close );
	}
	else if ( history_filename.find( "/." + custom + ".rpn" ) == std::string::npos )
	{
		// This seems somewhat disruptive - is this even an issue?
		//RPANY_THROW( rpany::stack_exception, "Linenoise history file already specified - can't use " + custom );
	}
}

void linenoise_history_show( rpany::stack &s )
{
	char *line = 0;
	for ( int index = 0; 0 != ( line = linenoiseHistoryLine( index ) ) ; index ++ )
	{
		s.output( ) << std::setw( 5 ) << index << ": " << line << std::endl;
		free( line );
	}
}

void linenoise_history_search( rpany::stack &s )
{
	auto needle = s.pull< std::string >( );
	char *line = 0;
	for ( int index = 0; 0 != ( line = linenoiseHistoryLine( index ) ) ; index ++ )
	{
		if ( std::string( line ).find( needle ) != std::string::npos )
			s.output( ) << std::setw( 5 ) << index << ": " << line << std::endl;
		free( line );
	}
}

RPANY_OPEN( linenoise, rpany::stack &s )
	if ( &s.input( ) == &std::cin && rpany::is_tty( ) )
	{
		static bool init = true;
		if ( init )
		{
			linenoiseSetCompletionCallback( completionHook );
			linenoiseHistorySetMaxLen( 500 );
			init = false;
		}
		s.teach( "prompt", [ ] ( rpany::stack &s ) { s += rl_gets( s.pull< std::string >( ) ); } );
		s.teach( "eof", [ ] ( rpany::stack &s ) { s += eof || !s.input( ); } );
		s.teach( "ss-complete", [ ] ( rpany::stack &s ) { s << "ss-dictionary"; completer_dictionary = s.pull< rpany::dictionary::dictionary_type_ptr >( 0 ); } );
		s.teach( "linenoise-dict", get_dictionary );
		s.teach( "report-max-width", [ ] ( rpany::stack &s ) { s += getScreenColumns( ); } );
		s.teach( "report-max-height", [ ] ( rpany::stack &s ) { s += getScreenRows( ); } );
		s.teach( "history-show", linenoise_history_show );
		s.teach( "history-search", linenoise_history_search );
		s.teach( "history-load", linenoise_history_load );
		s.teach( "history-custom-load", linenoise_history_custom_load );
	}
RPANY_CLOSE( linenoise );
