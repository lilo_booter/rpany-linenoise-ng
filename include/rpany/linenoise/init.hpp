#pragma once

bool _rpany_comms_enrolled = rpany::enroll( "rpany-linenoise" );

bool eof = false;

std::weak_ptr< rpany::dictionary::dictionary_type > completer_dictionary;

typedef std::set< std::string > lookup_type;
typedef std::map< std::string, lookup_type > completer_lookup_type;

completer_lookup_type completer_lookup;

std::string history_filename;
