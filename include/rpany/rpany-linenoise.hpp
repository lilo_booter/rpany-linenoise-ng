#pragma once

#include <rpany/rpany.hpp>

#include <set>

#if !defined( WIN32 ) && !defined( _WIN32 )
#include <dirent.h>
#endif

#include "linenoise/ConvertUTF.h"
#include "linenoise/ConvertUTF.cpp"
#include "linenoise/linenoise.h"
#include "linenoise/wcwidth.cpp"
#include "linenoise/linenoise.cpp"

namespace rpany_linenoise {
#include "linenoise/init.hpp"
#include "linenoise/readline.hpp"
}
