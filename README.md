# rpany-linenoise

A mixin for [rpany] which provides shells with a command line history using [linenoise-ng].

## Usage

To get a list of everything rpany-linenoise knows:

```
rpany-linenoise
```

See below for the details on the specific vocabs and words which are introduced.

To activate as a mixin with another rpany project, it is necessary to include
the `rpany-linenoise.hpp` header in the same file as `rpany-linenoise.hpp`

```
#include <rpany/rpany-host.hpp>
#include <rpany/rpany-linenoise.hpp>
```

And introduce it in your CMakeLists.txt like:

```
find_package( rpany-linenoise REQUIRED )
target_link_libraries( YOUR-TARGET PRIVATE rpany::linenoise::headerlib-linenoise )
```

Alternatively, it can be introduced as an optional mixin using:

```
#include <rpany/rpany-host.cpp>
#if defined( WITH_RPANY_LINENOISE )
#include <rpany/rpany-linenoise.cpp>
#endif
```

with the corresponding modification to cmake:

```
find_package( rpany-linenoise )
if ( rpany-linenoise_FOUND )
	target_compile_definitions( YOUR-TARGET PRIVATE WITH_RPANY_LINENOISE )
	target_link_libraries( YOUR-TARGET PRIVATE rpany::linenoise::headerlib-linenoise )
endif( )
```

Once recompiled, the hosting executables will be cognisant of the rpany-linenoise
functionality.

## Prequisites

* [cmake]
* c++11 compiler
* [rpany]

## Details

This mixin provides a general mechanism to provide basic line editing and history
in shell type environments.

As such, it mostly provides replacements for existing io functionality while adding
an additional mechanism for tab completion within shell environments.

By default, tab completion will offer all words which start with the space delimited
string immediately to the left the cursor, but a more flexible variation exists which
allows the user to navigate the vocabularies, using `::vocab-\>word` type of syntax.

The completed word functions identically to the 'word' suffix of the completed
string.

[cmake]: https://cmake.org/
[rpany]: https://gitlab.com/lilo_booter/rpany/blob/master/README.md
[linenoise-ng]: https://github.com/arangodb/linenoise-ng
